usuario="`whoami`"
url_git=""
vacio=""
IFS=' '
if [ $usuario = "root" ]
then
    ping -c1 google.com > /dev/null 2> /dev/null &> /dev/null 
    if [ $? -eq 0 ]; 
    then
        prueba=`echo $prueba`

        contador=0
        while read -r line
        do
            
            echo "Se esta instalando el repositorio $line" >> `echo $prueba`/salidaEntandard.out 
            add-apt-repository -y $line > /dev/null 2> /dev/null
            echo "Se ha acabado de instalar el repositorio $line" >> `echo $prueba`/salidaEntandard.out 
            contador=contador+1 

        done < $prueba/archivosApt/repositorios.txt

        apt-get update > /dev/null 2> /dev/null
        fuser -vka /var/lib/dpkg/lock-frontend > /dev/null 2> /dev/null
        fuser -vka /var/lib/dpkg/lock > /dev/null 2> /dev/null

        contador=0
        while read -r line
        do
            
            echo "Se esta instalando el paquete $line" >> `echo $prueba`/salidaEntandard.out 
            apt-get -y install $line > /dev/null 2> /dev/null
            echo "Se ha acabado de instalar el paquete $line" >> `echo $prueba`/salidaEntandard.out 
            contador=contador+1 

        done < $prueba/archivosApt/instalarAPT.txt
        
        contador=0
        while read -r line
        do
            url=`echo $line | cut -d " " -f 1`
            nombre=`echo $line | cut -d " " -f 2`

            echo "Se esta instalando el paquete $nombre" >> `echo $prueba`/salidaEntandard.out 
            wget -q -O $nombre $url > /dev/null 2> /dev/null
            cp $nombre `echo $raiz`
            chmod +x `echo $raiz`/$nombre
            echo "Se ha acabado de instalar el paquete $nombre" >> `echo $prueba`/salidaEntandard.out 

            contador=contador+1 

        done < $prueba/archivosApt/descargar.txt

        fuser -vka /var/lib/dpkg/lock-frontend > /dev/null 2> /dev/null
        fuser -vka /var/lib/dpkg/lock > /dev/null 2> /dev/null
        
        contador=0
        while read -r line
        do
            url=`echo $line | cut -d " " -f 1`
            nombre=`echo $line | cut -d " " -f 2`

            echo "Se esta instalando el paquete $nombre" >> `echo $prueba`/salidaEntandard.out 
            wget -q -O $nombre $url > /dev/null 2> /dev/null
            #dpkg -i `echo $prueba`/$1 > /dev/null 2> /dev/null
            dpkg -i `echo $prueba`/$nombre
            echo "Se ha acabado de instalar el paquete $nombre" >> `echo $prueba`/salidaEntandard.out 

            contador=contador+1 

        done < $prueba/archivosDEB/deb.txt

        apt-get -f -y install > /dev/null 2> /dev/null

    else
        echo "\e[31mError. No tienes conexion a internet.\e[0m"
    fi
        
else
    echo "\e[31mError. Estas ejecutando como el usuario\e[0m \e[44m$usuario\e[0m\e[31m. Tienes que hacerlo como el usuario \e[0m\e[42mroot.\e[0m"
fi
