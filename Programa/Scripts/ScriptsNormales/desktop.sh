n=1
filename="desktop.txt"
separador=";"
raiz="$1"

while read line; do

    nombreArchivo=`echo $line | cut -d $separador -f 1`
    nombreMenu=`echo $line | cut -d $separador -f 2`
    descripcion=`echo $line | cut -d $separador -f 3`
    script=$raiz/`echo $line | cut -d $separador -f 4`
    icono=`echo $line | cut -d $separador -f 5`
    
    echo "[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=$nombreMenu
Comment=$descripcion
Exec=$script" > "/usr/share/applications/$nombreArchivo"

    if [ ! -z "$icono" ];
    then
        echo "Icon=$raiz$icono" >> "/usr/share/applications/$nombreArchivo"
    fi

    echo "Terminal=false" >> "/usr/share/applications/$nombreArchivo"

    n=$((n+1))

done < $2
