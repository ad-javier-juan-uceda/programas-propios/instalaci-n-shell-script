n=1
filename="desktop.txt"
separador=";"
raiz="$1"
file="`echo $HOME`/.local/share/applications/"

while read line; do

    nombreArchivo=`echo $line | cut -d $separador -f 1`
    nombreMenu=`echo $line | cut -d $separador -f 2`
    descripcion=`echo $line | cut -d $separador -f 3`
    script=$raiz/`echo $line | cut -d $separador -f 4`
    icono=`echo $line | cut -d $separador -f 5`
    
    echo "[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=$nombreMenu
Comment=$descripcion
Exec=$script" > "$file$nombreArchivo"

    if [ ! -z "$icono" ];
    then
        echo "Icon=$raiz$icono" >> "$file$nombreArchivo"
    fi

    echo "Terminal=false" >> "$file$nombreArchivo"

    n=$((n+1))

done < $2

update-desktop-database $file
