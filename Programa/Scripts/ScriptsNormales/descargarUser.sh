usuario="`whoami`"
url_git=""
vacio=""
IFS=' '
if [ $usuario != "root" ]
then
    ping -c1 google.com > /dev/null 2> /dev/null &> /dev/null 
    if [ $? -eq 0 ]; 
    then 
        if [ $# -eq 2 ];
            then
            if [ "`which wget`" != "" ] 2> /dev/null;
            then
                echo >> `echo $prueba`/salidaEntandard.out
                echo "Comenzando descarga con el nombre $1" >> `echo $prueba`/salidaEntandard.out
                wget -q -O $1 $2 && echo "Descarga completada con el nombre $1" >> `echo $prueba`/salidaEntandard.out
                            	
            else
                echo "\e[31mError. El paquete wget no esta instalado.\e[0m"
            fi
        else
            echo "\e[31mError. Solo se puede ejecutar con un parametro que referencie a un archivo.\e[0m"
        fi
    else
        echo "\e[31mError. No tienes conexion a internet.\e[0m"
    fi
        
else
    echo "\e[31mError. Estas ejecutando como el usuario\e[0m \e[44m$usuario\e[0m\e[31m. Tienes que hacerlo como el usuario \e[0m\e[42mroot.\e[0m"
fi
