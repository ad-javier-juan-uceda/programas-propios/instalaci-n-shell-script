usuario="`whoami`"
url_git=""
vacio=""
IFS=' '
if [ $usuario != "root" ]
then
    ping -c1 google.com > /dev/null 2> /dev/null &> /dev/null 
    if [ $? -eq 0 ]; 
    then 
        if [ $# -eq 1 ];
            then
            if [ -d $1 ];
            then
	    	if [ -r $1 ];
                then
                    if [ -w $1 ];
                    then
                        if [ -x $1 ];
                        then
                            if [ "`which git`" != "" ] 2> /dev/null;
                            then

                                export prueba="`pwd`"
                                export raiz="$1"
                            	# Aqui ira todo lo que hay que hacer.      
                                echo > `echo $prueba`/salidaEntandard.out 

                                mkdir `echo $iconos`
				               # cp -R  `echo $prueba`/archivosDesktop/* /usr/share/applications
				                #cp -R  `echo $prueba`/Iconos/* `echo $iconos`
                            	contador=0

                                while read -r line
                                do

                                    url=`echo $line | cut -d " " -f 1`
                                    nombre=`echo $line | cut -d " " -f 2`
                                    chmod +x ./Scripts/ScriptsNormales/descargarUser.sh
                                    bash ./Scripts/ScriptsNormales/archivosExtraiblesUser.sh $nombre $url &

                                    contador=contador+1 

                                done < `pwd`/archivosExtraibles/archivosExtraibles.txt
                                
                                #apt install unzip bzip2 -y
                            	contador=0

                                while read -r line
                                do

                                    url=`echo $line | cut -d " " -f 1`
                                    nombre=`echo $line | cut -d " " -f 2`
                                    chmod +x ./Scripts/ScriptsNormales/descargarUser.sh
                                    bash ./Scripts/ScriptsNormales/archivosExtraiblesZIPUser.sh $nombre $url &

                                    contador=contador+1 

                                done < `pwd`/archivosZIP/zip.txt
                                
                                bash `echo $prueba`/Scripts/ScriptsNormales/desktopUser.sh `echo $raiz` `echo $prueba`/archivosDesktop/desktop.txt &
                                tail -f `echo $prueba`/salidaEntandard.out   
                            	
                            else
                            	echo "\e[31mError. El paquete git no esta instalado.\e[0m"
                            fi
                        else
                            echo "\e[31mError. El archivo $1 no tiene permisos de ejecucion.\e[0m"
                        fi
                    else
                        echo "\e[31mError. El archivo $1 no tiene permisos de escritura.\e[0m"
                    fi
                else
                    echo "\e[31mError. El archivo $1 no tiene permisos de lectura.\e[0m"
                fi
            else
                echo "\e[31mError. El archivo $1 no existe o no existe y no es un directorio.\e[0m"
            fi
        else
            echo "\e[31mError. Solo se puede ejecutar con un parametro que referencie a un archivo.\e[0m"
        fi
    else
        echo "\e[31mError. No tienes conexion a internet.\e[0m"
    fi
        
else
    echo "\e[31mError. Estas ejecutando como el usuario\e[0m \e[44m$usuario\e[0m\e[31m. Tienes que hacerlo como el usuario \e[0m\e[42mroot.\e[0m"
fi
